package com.sumadireja.layouting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickStart2(View v) {
        Intent i = (new Intent(MainActivity.this, Start.class));
        startActivity(i);
    }

    public void klikMulai(View v) {
        Intent i = (new Intent(MainActivity.this, Biasa.class));
        startActivity(i);
    }

    public void klikStart(View v) {
        Intent i = (new Intent(MainActivity.this, PersegiPanjang.class));
        startActivity(i);
    }
}

