package com.sumadireja.layouting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


/**
 * Created by sumadireja on 3/26/2018.
 */

public class Start extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
    }

    public void klikMulai(View v) {
        Intent i = (new Intent(Start.this, Biasa.class));
        startActivity(i);
    }

    public void klikStart(View v) {
        Intent i = (new Intent(Start.this, PersegiPanjang.class));
        startActivity(i);
    }

    public void klikLihat(View v) {
        Intent i = (new Intent(Start.this, Biodata.class));
        startActivity(i);

    }
}