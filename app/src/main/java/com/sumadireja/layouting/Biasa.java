package com.sumadireja.layouting;

import android.os.Bundle;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import org.w3c.dom.Text;


/**
 * Created by sumadireja on 3/26/2018.
 */

public class Biasa extends AppCompatActivity {

    private double hasilAkhir = 0;
    private String angka1 = "";
    private String angka2 = "";
    TextView hasil;
    Button btJumlah, btKurang, btKali, btBagi;
    EditText edAngka1, edAngka2;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biasa);

        edAngka1=(EditText)findViewById(R.id.editAngka1);
        edAngka2 =(EditText)findViewById(R.id.editAngka2);
        hasil = (TextView) findViewById(R.id.textHasil);
        btJumlah = (Button) findViewById(R.id.tombolTambah);
        btKurang = (Button) findViewById(R.id.tombolKurang);
        btKali = (Button) findViewById(R.id.tombolKali);
        btBagi = (Button) findViewById(R.id.tombolBagi);
    }

    public void klikTambah(View V) {
        angka1 = edAngka1.getText().toString();
        angka2 = edAngka2.getText().toString();

        if (angka1.equalsIgnoreCase("") || (angka2.equalsIgnoreCase(""))) {
            edAngka1.setFocusable(true);
        } else {
            double angkaA = Double.parseDouble(angka1);
            double angkaB = Double.parseDouble(angka2);
            hasilAkhir = angkaA + angkaB;
            String hasilString = String.valueOf(hasilAkhir);
            hasil.setText(hasilString);
        }
    }

    public void klikKurang(View V) {
        angka1 = edAngka1.getText().toString();
        angka2 = edAngka2.getText().toString();

        if (angka1.equalsIgnoreCase("") || (angka2.equalsIgnoreCase(""))) {
            edAngka1.setFocusable(true);
        } else {
            double angkaA = Double.parseDouble(angka1);
            double angkaB = Double.parseDouble(angka2);
            hasilAkhir = angkaA - angkaB;
            String hasilString = String.valueOf(hasilAkhir);
            hasil.setText(hasilString);
        }
    }

    public void klikKali(View V) {
        angka1 = edAngka1.getText().toString();
        angka2 = edAngka2.getText().toString();

        if (angka1.equalsIgnoreCase("") || (angka2.equalsIgnoreCase(""))) {
            edAngka1.setFocusable(true);
        } else {
            double angkaA = Double.parseDouble(angka1);
            double angkaB = Double.parseDouble(angka2);
            hasilAkhir = angkaA * angkaB;
            String hasilString = String.valueOf(hasilAkhir);
            hasil.setText(hasilString);
        }
    }

    public void klikBagi(View V) {
        angka1 = edAngka1.getText().toString();
        angka2 = edAngka2.getText().toString();

        if (angka1.equalsIgnoreCase("") || (angka2.equalsIgnoreCase(""))) {
            edAngka1.setFocusable(true);
        } else {
            double angkaA = Double.parseDouble(angka1);
            double angkaB = Double.parseDouble(angka2);
            hasilAkhir = angkaA / angkaB;
            String hasilString = String.valueOf(hasilAkhir);
            hasil.setText(hasilString);
        }
    }
}