package com.sumadireja.layouting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Button;
import android.view.View;


/**
 * Created by sumadireja on 3/27/2018.
 */

public class Biodata extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biodata);
    }

    public void klikBack(View v) {
        Intent i = (new Intent(Biodata.this, Start.class));
        startActivity(i);
    }
}
