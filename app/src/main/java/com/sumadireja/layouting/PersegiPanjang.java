package com.sumadireja.layouting;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


/**
 * Created by sumadireja on 3/26/2018.
 */

public class PersegiPanjang extends AppCompatActivity {
    private EditText editText;
    private EditText editText2;
    private EditText editText3;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.persegipanjang);
        initUI();
        initEvent();
    }

    private void initUI() {
        editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        button = (Button) findViewById(R.id.button);

    }

    private void initEvent() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitungluas();
            }
        });
    }

    public void hitungluas(){
        int panjang = Integer.parseInt(editText.getText().toString());
        int lebar = Integer.parseInt(editText2.getText().toString());
        int luas = panjang * lebar;
        editText3.setText(luas+"");
    }
}